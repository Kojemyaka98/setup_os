#!/bin/bash

SHOULD_DOWNLOAD_IMAGE="true"
SHOULD_MOUNT_DISK="true"
SHOULD_UNMOUNT_DISK="true"
DIR_IMAGE="/home/script/images"

download_os() {
   echo ">>> Starting download system..."
    cd $DIR_IMAGE && wget https://releases.ubuntu.com/20.04/ubuntu-20.04.4-live-server-amd64.iso \
	sleep 5 
	echo "Download OS completed"
}

mount_img() {

#   mount $DIR_IMAGE/ubuntu-20.04.4-live-server-amd64.iso /mnt/
#   mkdir /tmp/isoubuntu
#   cp -R /mnt/* /tmp/isoubuntu && cd isoubuntu
#   cp /root/.ssh/authorizedkeys /tmp/isoubuntu

rm -rf $BUILD/
mkdir $BUILD/
echo ">>> Mounting image..."
mount -o loop $IMAGE /mnt/
echo ">>> Syncing..."
rsync -av /mnt/ $BUILD/
chmod -R u+w $BUILD/

}

unmount_img() {

CUSTOM_IMAGE=ubuntu-custom.iso
# Запаковываем содержимое iso/ в образ ubuntu-custom.iso
echo ">>> Calculating MD5 sums..."
rm $BUILD/md5sum.txt
(cd $BUILD/ && find . -type f -print0 | xargs -0 md5sum | grep -v "boot.cat" | grep -v "md5sum.txt" > md5sum.txt)
echo ">>> Building iso image..."

mkisofs -r -V "Ubuntu OEM install" \
            -cache-inodes \
            -J -l -b isolinux/isolinux.bin \
            -c isolinux/boot.cat -no-emul-boot \
            -boot-load-size 4 -boot-info-table \
            -o $CUSTOM_IMAGE $BUILD/

umount -f /mnt/

}



vish_vm {
echo ">>> Starting to create Virtual Machine..."

sudo virt-install \
--virt-type=kvm \
--name=ubuntu-20.04 \
--ram=2048 \
--vcpus=2 \
--os-variant=ubuntu20.04 \
--hvm \
--cdrom=/var/lib/libvirt/boot/ubuntu-20.04-server-cloudimg-amd64-disk-kvm.img \
--network=bridge:virbr0,model=virtio \
--graphics vnc \
--disk path=/var/lib/libvirt/images/ubuntu20.qcow2,size=40,bus=virtio,format=qcow2


}

if [ "${SHOULD_DOWNLOAD_IMAGE}" == "true" ]; then
check_img=`ls $DIR_IMAGE | grep "ubuntu-20" | wc -l`
echo $check_img

if [ "$check_img" = "1" ]; then
   echo "ISO image  exist, next step....."
else
   echo "ISO image doesn't exist, start downloading the image...."
   download_os
fi
fi



if [ "${SHOULD_MOUNT_DISK}" == "true" ]; then
   mount_img
fi

if [ "${SHOUD_UNMOUNT_DISK}" == "true" ]; then
   umount_img
fi

echo "---- VM setup is done ----"
